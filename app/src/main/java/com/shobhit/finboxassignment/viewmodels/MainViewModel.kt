package com.shobhit.finboxassignment.viewmodels

import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.shobhit.finboxassignment.database.DataBase
import com.shobhit.finboxassignment.database.Location
import com.shobhit.finboxassignment.database.LocationDao
import com.shobhit.finboxassignment.models.DataState
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class MainViewModel @ViewModelInject constructor(
    @Assisted
    private val savedStateHandle: SavedStateHandle,
    @ApplicationContext val context: Context,
    private val locationRequest: LocationRequest,
    private val dao: DataBase
) : ViewModel() {

    val tag: String = MainViewModel::class.java.simpleName
    val enableLocation: MutableLiveData<DataState<Boolean>> = MutableLiveData()
    val storedLocation: MutableLiveData<List<Location>> = MutableLiveData()
    val isLocationAvailable : ObservableField<Boolean> = ObservableField()

    /**
     * function to get all the stored location in db
     */
    fun getStoredLocation(){
        viewModelScope.async(Dispatchers.IO){
            val locations = dao.locationDao().selectAll()
            withContext(Dispatchers.Main){
                storedLocation.value = locations
            }
        }
    }

    /**
     * function to setup the location request for fetching location
     */
    fun locationSetup() {
        enableLocation.value = DataState.Loading
        LocationServices.getSettingsClient(context).checkLocationSettings(
            LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
                .setAlwaysShow(true)
                .build()
        )
            .addOnSuccessListener { enableLocation.value = DataState.Success(true) }
            .addOnFailureListener {
                enableLocation.value = DataState.Error(it)
                Log.e(tag, "some error setting up location : ", it)

            }

    }
}