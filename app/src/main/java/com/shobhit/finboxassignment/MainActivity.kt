package com.shobhit.finboxassignment

import android.Manifest
import android.content.IntentSender
import android.os.Build
import android.os.Bundle
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.google.android.gms.common.api.ResolvableApiException
import com.shobhit.finboxassignment.adapter.LocationAdapter
import com.shobhit.finboxassignment.data.LocationRepo
import com.shobhit.finboxassignment.databinding.ActivityMainBinding
import com.shobhit.finboxassignment.models.DataState
import com.shobhit.finboxassignment.ui.BaseActivity
import com.shobhit.finboxassignment.util.Constant.Companion.DEFAULT_MIN_INTERVAL
import com.shobhit.finboxassignment.util.Constant.Companion.REQUEST_CHECK_SETTINGS
import com.shobhit.finboxassignment.util.UpdateLocationWorker
import com.shobhit.finboxassignment.viewmodels.MainViewModel
import com.tbruyelle.rxpermissions2.RxPermissions
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.observers.DisposableObserver
import java.util.concurrent.TimeUnit
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>() {
    private val tag = MainActivity::class.java.simpleName
    private val viewModel: MainViewModel by viewModels()
    lateinit var adapter: LocationAdapter

    @Inject
    lateinit var locationRepo: LocationRepo

    override fun getBindingVariable(): Int {
        return BR.mainVm
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun getViewModelBase(): MainViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeData()
        initRecy()
    }

    /**
     * initialize the recyclerview and the adapter as well
     */
    private fun initRecy() {
        with(mViewDataBinding.locationRecy) {
            layoutManager = LinearLayoutManager(this@MainActivity)
            setHasFixedSize(true)
        }
    }

    private fun observeData() {
        viewModel.enableLocation.observe(this, {
            when (it) {
                is DataState.Success -> {
                    scheduleLOcationUpdateJob()
                }
                is DataState.Error -> {
                    if (it.exception is ResolvableApiException) {
                        try {
                            it.exception.startResolutionForResult(
                                this@MainActivity,
                                REQUEST_CHECK_SETTINGS
                            )
                        } catch (sendEx: IntentSender.SendIntentException) {
                            sendEx.printStackTrace()
                        }
                    }
                }
                DataState.Loading -> {
                    // handle ui while loading
                }
            }
        })

        viewModel.storedLocation.observe(this, Observer {
            it?.let {
                viewModel.isLocationAvailable.set(it.isNotEmpty())
                with(mViewDataBinding.locationRecy){
                    val adapter = LocationAdapter(it)
                    setAdapter(adapter)
                }
            }
        })

    }

    override fun onResume() {
        super.onResume()
        checkForLocationPermissionAndInitiateLocation()
        viewModel.getStoredLocation()
    }



    private fun checkForLocationPermissionAndInitiateLocation() {
        val rxPermissions = RxPermissions(this)
        rxPermissions.request(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION
        )
            .subscribe(object : DisposableObserver<Boolean?>() {
                override fun onNext(aBoolean: Boolean) {
                    if (aBoolean) {
                        viewModel.locationSetup()
                    }
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                }
                override fun onComplete() {}
            })
    }

    private fun scheduleLOcationUpdateJob() {
        val job = PeriodicWorkRequestBuilder<UpdateLocationWorker>(
            DEFAULT_MIN_INTERVAL,
            TimeUnit.MINUTES
        )
            .addTag(UpdateLocationWorker::class.java.simpleName)
            .build()

        WorkManager.getInstance(this).enqueueUniquePeriodicWork(
            UpdateLocationWorker::class.java.simpleName,
            ExistingPeriodicWorkPolicy.KEEP, job
        )
    }
}