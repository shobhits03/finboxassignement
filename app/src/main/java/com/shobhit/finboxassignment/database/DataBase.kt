package com.shobhit.finboxassignment.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [
        Location::class
    ], version = 1
)
abstract class DataBase : RoomDatabase() {
    abstract fun locationDao(): LocationDao
}