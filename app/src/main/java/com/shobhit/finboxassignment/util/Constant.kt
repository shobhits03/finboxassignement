package com.shobhit.finboxassignment.util

class Constant {

    companion object {
        const val REQUEST_CHECK_SETTINGS = 100
        const val DEFAULT_MIN_INTERVAL = 60L
    }
}