package com.shobhit.finboxassignment.data

import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.LocationManager
import android.util.Log
import android.widget.Toast

import com.google.android.gms.location.LocationServices
import com.shobhit.finboxassignment.database.DataBase
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocationRepo @Inject constructor(
    @ApplicationContext private val context: Context,
    private val database: DataBase
) {

    private val tag = LocationRepo::class.java.simpleName
    fun isGPSEnabled() = (context.getSystemService(LOCATION_SERVICE) as LocationManager)
        .isProviderEnabled(LocationManager.GPS_PROVIDER)

    fun checkLocationPermission(): Boolean =
        context.checkCallingOrSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED


    fun getLocation() {
        Log.d(
            tag,
            "isGPSEnabled : " + isGPSEnabled() + "  checkLocationPermission  " + checkLocationPermission()
        )
        if (isGPSEnabled() && checkLocationPermission()) {
            LocationServices.getFusedLocationProviderClient(context)
                ?.lastLocation
                ?.addOnSuccessListener { location: android.location.Location? ->
                    if (location != null) {
                        Log.d(
                            tag,
                            "location found : " + location.latitude + ":" + location.longitude
                        )
                        saveLocation(com.shobhit.finboxassignment.database.Location(0, location.latitude, location.longitude, System.currentTimeMillis()))
                    }else{
                        Log.e(tag,"unable to get location. aborting...")
                    }
                }
        } else {
            // can handle in multiple ways. Kept it simple for now
            Toast.makeText(
                context,
                "Either gps is turned off or permission is not granted",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun saveLocation(location: com.shobhit.finboxassignment.database.Location) = GlobalScope.launch(Dispatchers.IO) {
        Log.d(tag,"data inserted : "+database.locationDao().insert(location))
    }
}