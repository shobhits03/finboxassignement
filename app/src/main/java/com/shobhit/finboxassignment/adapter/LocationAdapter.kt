package com.shobhit.finboxassignment.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.shobhit.finboxassignment.R
import com.shobhit.finboxassignment.database.Location
import com.shobhit.finboxassignment.databinding.LocationItemBinding
import java.text.SimpleDateFormat
import java.util.*

class LocationAdapter(private val locationList: List<Location>)
    : RecyclerView.Adapter<LocationAdapter.ViewHolder>() {
    private var inflater: LayoutInflater? = null

    class ViewHolder(locationItemBinding: LocationItemBinding) :
        RecyclerView.ViewHolder(locationItemBinding.root) {
        var locationItemBinding: LocationItemBinding? = null

        init {
            this.locationItemBinding = locationItemBinding;
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (inflater == null) inflater = LayoutInflater.from(parent.context)
        val binding: LocationItemBinding =
            DataBindingUtil.inflate(inflater!!, R.layout.location_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = locationList[position]
        holder.locationItemBinding?.run {
            location.text = data.latitude.toString().plus(" : ").plus(data.longitude)
            time.text = getDate(data.timestamp,"dd/MM/yyyy hh:mm:ss")
        }
    }

    override fun getItemCount(): Int {
        return locationList.size
    }

    @SuppressLint("SimpleDateFormat")
    fun getDate(milliSeconds: Long, dateFormat: String?): String? {
        val formatter = SimpleDateFormat(dateFormat)
        val calendar: Calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }
}