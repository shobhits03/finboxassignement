package com.shobhit.finboxassignment.database

import androidx.room.*
import io.reactivex.Flowable

@Dao
interface LocationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(location: Location): Long

    @Query("SELECT * FROM location")
    fun selectAll(): List<Location>
}