package com.shobhit.finboxassignment.ui


import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel


abstract class BaseActivity<V : ViewModel, T : ViewDataBinding> : AppCompatActivity() {

    lateinit var mViewDataBinding: T
    var mViewModel: V? = null

    private val tag = BaseActivity::class.java.simpleName

    abstract fun getBindingVariable(): Int

    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun getViewModelBase(): V


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDataBinding()
    }

    /**
     * perform global binding
     */
    open fun performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        mViewModel = mViewModel ?: getViewModelBase()
        mViewDataBinding.apply {
            setVariable(getBindingVariable(), mViewModel)
            lifecycleOwner = this@BaseActivity
            executePendingBindings()
        }
    }
}