package com.shobhit.finboxassignment.util

import android.content.Context
import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.work.WorkerInject
import androidx.work.CoroutineWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.shobhit.finboxassignment.data.LocationRepo
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UpdateLocationWorker @WorkerInject constructor(
    @Assisted context: Context,
    @Assisted workerParams: WorkerParameters,
    var repository: LocationRepo
) : CoroutineWorker(context, workerParams) {
    private val tag = UpdateLocationWorker::class.java.simpleName

    override suspend fun doWork(): Result {
        return withContext(Dispatchers.IO) {
            try {
                repository.getLocation()
                Result.success()
            } catch (e: Exception) {
                Log.d(tag, "Exception getting location -->  ${e.message}")
                Result.failure()
            }
        }
    }
}