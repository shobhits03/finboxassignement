package com.shobhit.finboxassignment.di

import android.app.Application

import androidx.room.Room
import com.google.android.gms.location.LocationRequest
import com.shobhit.finboxassignment.database.DataBase
import com.shobhit.finboxassignment.database.Location
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun locationRequest(): LocationRequest = LocationRequest.create().apply {
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        interval = 3 * 1000
        fastestInterval = 5 * 1000
    }

    @Provides
    @Singleton
    fun provideDatabase(application: Application): DataBase =
        Room.databaseBuilder(application, DataBase::class.java, "FinBox.db").build()

}